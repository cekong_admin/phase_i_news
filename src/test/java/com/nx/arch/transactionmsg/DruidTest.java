package com.nx.arch.transactionmsg;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.druid.filter.logging.Slf4jLogFilter;
import com.alibaba.druid.pool.DruidDataSource;

public class DruidTest {

    private static String mysqlUrl = "jdbc:mysql://127.0.0.1:13306/arch_config?zeroDateTimeBehavior=convertToNull&logger=com.mysql.jdbc.log.Slf4JLogger";
    private static final Logger log = LoggerFactory.getLogger(DruidTest.class);
    @Test
    public void testConnection() throws SQLException{
        // 配置真实数据源
        log.info("*****************start helloworld ****************");
        Map<String, DataSource> dataSourceMap = new HashMap<>(10);
        Slf4jLogFilter filter = new Slf4jLogFilter();
        filter.setConnectionLogEnabled(true);
        // 配置第一个数据源
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl(mysqlUrl);
        dataSource.setUsername("root");
        dataSource.setPassword("123456");
        dataSource.setMinIdle(5);
        dataSource.setInitialSize(1);
        dataSource.setMaxActive(5);
        dataSource.setRemoveAbandoned(true);
        dataSource.setRemoveAbandonedTimeout(10);
        dataSource.setMaxWait(1000);
        dataSource.setLogAbandoned(true);
        // list.add(filter);
       // dataSource.setProxyFilters(list);
       // dataSource.init();
        dataSourceMap.put("ds0", dataSource);
        Connection con = dataSource.getConnection();
        System.out.println("connection "+con.getAutoCommit());
        System.out.println("cru"+System.currentTimeMillis());
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
/*        for(;;){
            try {
                Connection tmp =  dataSource.getConnection();
                TimeUnit.MILLISECONDS.sleep(1);
                //tmp.close();
            } catch (Exception e) {
                // TODO Auto-generated catch block
               // e.printStackTrace();
            }
        }*/
    }
}
