package com.nx.arch.transactionmsg;

import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;

import org.junit.Test;

import com.nx.arch.transactionmsg.model.Msg;

public class PriorityBlockingQueueTest {
    
    @Test
    public void testOrderByTime() {
        PriorityBlockingQueue<Msg> queue = new PriorityBlockingQueue<Msg>(100, new Comparator<Msg>() {
            @Override
            public int compare(Msg o1, Msg o2) {
                // TODO Auto-generated method stub
                long diff = o1.getCreateTime() - o2.getCreateTime();
                if (diff > 0) {
                    return 1;
                } else if (diff < 0) {
                    return -1;
                }
                return 0;
            }
        });
        Msg msg1 = new Msg(1L, "url1");
        msg1.setCreateTime(3);
        queue.put(msg1);
        Msg msg2 = new Msg(2L, "url2");
        msg2.setCreateTime(2);
        queue.put(msg2);
        Msg msg3 = new Msg(3L, "url3");
        msg3.setCreateTime(2);
        queue.put(msg3);
        Msg msg4 = new Msg(4L, "url3");
        queue.put(msg4);
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
    }
}
